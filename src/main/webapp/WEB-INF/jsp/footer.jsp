<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<div id="footer" class="container" style="margin: 0 auto;">
    <div id="f">
        <dl class="f_l">
            <dt class="f_t"><a href="">书海风云</a></dt>
            <dd class="f_d"><a href="" title="图书排名">图书排名</a></dd>
            <dd class="f_d"><a href="" title="图书推荐">图书推荐</a></dd>
            <dd class="f_d"><a href="" title="图书人气">图书人气</a></dd>
        </dl>
        <dl class="f_l">
            <dt class="f_t"><a href="">经典国学</a></dt>
            <dd class="f_d"><a href="" title="易经">易经</a></dd>
            <dd class="f_d"><a href="" title="道德经">道德经</a></dd>
            <dd class="f_d"><a href="" title="黄帝内经">黄帝内经</a></dd>
        </dl>
        <dl class="f_l">
            <dt class="f_t"><a href="">会员中心</a></dt>
            <dd class="f_d"><a href="" title="资金管理">资金管理</a></dd>
            <dd class="f_d"><a href="" title="特权礼包">特权礼包</a></dd>
            <dd class="f_d"><a href="" title="商域">商域</a></dd>
        </dl>
        <dl class="f_l">
            <dt class="f_t"><a href="">安全中心</a></dt>
            <dd class="f_d"><a href="" title="帐号管理">帐号管理</a></dd>
            <dd class="f_d"><a href="" title="防盗号">防盗号</a></dd>
            <dd class="f_d"><a href="" title="充值">充值</a></dd>
        </dl>
        <dl class="f_l">
            <dt class="f_t"><a href="">联系我们</a></dt>
            <dd class="f_d"><a href="" title="网站故障报告">网站故障报告</a></dd>
            <dd class="f_d"><a href="" title="游戏咨询">游戏咨询</a></dd>
            <dd class="f_d"><a href="" title="投诉与建议">投诉与建议</a></dd>
        </dl>
        
        <div class="clear"></div>
    </div>
   
    <div class="rear">
        <dl>
            
           
            <dt>
              
                <span><a href="">网站出版许可证|</a></span>
                <span><a href="">质量保证检验报告|</a></span>
                <span><a href="">内容健康证明|</a></span>
                <span><a href="">受国家保护文案|</a></span>
                <span><a href="">观众点击变化图</a></span>
            </dt>
            <dt><span>健康游戏公告：抵制不良游戏&nbsp;拒绝盗版游戏&nbsp;注意自我保护&nbsp;
								谨防受骗上当&nbsp; 适度游戏益脑&nbsp;沉迷游戏伤身&nbsp;合理安排时间&nbsp;享受健康生活</span></dt>
        </dl>
    </div>
</div>

</html>
